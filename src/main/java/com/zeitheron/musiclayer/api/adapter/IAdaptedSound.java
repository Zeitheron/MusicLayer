package com.zeitheron.musiclayer.api.adapter;

public interface IAdaptedSound
{
	/** [0;1] */
	boolean setVolume(double volume);
	
	/** [0;1] */
	double getVolume();
	
	/** [0;1] */
	boolean seek(double progress);
	
	/** [-1;1] */
	boolean setBalance(double balance);
	
	boolean play();
	
	boolean pause();
	
	boolean isPaused();
	
	boolean stop();
}