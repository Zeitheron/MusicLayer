package com.zeitheron.musiclayer.api.adapter;

import java.io.InputStream;

import com.zeitheron.hammercore.annotations.WIP;

@WIP
public interface ISoundAdapter
{
	IAdaptedSound createStreamingSound(InputStream stream);
}