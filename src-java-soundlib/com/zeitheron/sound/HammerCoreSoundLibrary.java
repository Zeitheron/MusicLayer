package com.zeitheron.sound;

import com.zeitheron.hammercore.api.HammerCoreAPI;
import com.zeitheron.hammercore.api.IHammerCoreAPI;
import com.zeitheron.hammercore.api.ILog;

@HammerCoreAPI(name = "SoundLib", version = "1.1.4")
public class HammerCoreSoundLibrary implements IHammerCoreAPI
{
	@Override
	public void init(ILog log, String version)
	{
		log.info("Loading Sound Library v" + version + "...");
		log.info("Sound Library Loaded! Supported type: Ogg");
	}
}