/*
 * Decompiled with CFR 0_123.
 */
package com.zeitheron.sound;

import javax.sound.sampled.FloatControl;

public interface ISound {
    public void setVolume(float var1);

    public boolean isDonePlaying();

    public void play();

    public void stop();

    public void dispose();

    public FloatControl getControl(FloatControl.Type var1);
}

