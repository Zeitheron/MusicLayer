package com.zeitheron.sound;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.FloatControl.Type;

public class Sound implements ISound
{
	final SourceDataLine line;
	final AudioInputStream ais;
	final AudioFormat outFormat;
	final Object lock = new Object();
	public final Object onFinished = new Object();
	boolean isPaused = true;
	boolean isDisposed = false;
	boolean isDonePlaying = false;
	
	public Sound(InputStream soundIn) throws UnsupportedAudioFileException, IOException, LineUnavailableException
	{
		this.ais = AudioSystem.getAudioInputStream(new BufferedInputStream(soundIn));
		this.outFormat = SoundLib.getOutFormat(this.ais.getFormat());
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, this.outFormat);
		this.line = (SourceDataLine) AudioSystem.getLine(info);
		this.line.open(this.outFormat);
		new Thread(() ->
		{
			try
			{
				this.line.start();
				this.appendAudioData(AudioSystem.getAudioInputStream(this.outFormat, this.ais));
				this.line.drain();
				this.line.stop();
				this.line.close();
			} catch(Exception e)
			{
				e.printStackTrace();
			}
		}).start();
	}
	
	public Sound(byte[] soundIn) throws UnsupportedAudioFileException, IOException, LineUnavailableException
	{
		this(new ByteArrayInputStream(soundIn));
	}
	
	@Override
	public void setVolume(float vol)
	{
		FloatControl volume = this.getControl(FloatControl.Type.MASTER_GAIN);
		float difference = volume.getMaximum() - volume.getMinimum();
		volume.setValue(volume.getMinimum() + difference * vol);
	}
	
	public float getVolumeF()
	{
		try
		{
			return (float) Math.pow(10, getControl(Type.MASTER_GAIN).getValue() / 20);
		} catch(IllegalArgumentException iae)
		{
			return 0F;
		}
	}
	
	public void setVolumeF(float vol)
	{
		/** Escape illegal values */
		if(vol < 0F || vol > 1F)
			return;
		try
		{
			getControl(Type.MASTER_GAIN).setValue(20F * (float) Math.log10(vol));
		} catch(IllegalArgumentException iae)
		{
		}
	}
	
	/* WARNING - Removed try catching itself - possible behaviour change. */
	@Override
	public void play()
	{
		if(!this.isPaused)
		{
			return;
		}
		this.isPaused = false;
		Object object = this.lock;
		synchronized(object)
		{
			this.lock.notifyAll();
		}
	}
	
	@Override
	public void stop()
	{
		this.isPaused = true;
	}
	
	/* WARNING - Removed try catching itself - possible behaviour change. */
	@Override
	public void dispose()
	{
		this.stop();
		this.isDisposed = true;
		Object object = this.lock;
		synchronized(object)
		{
			this.lock.notifyAll();
		}
	}
	
	@Override
	public boolean isDonePlaying()
	{
		return this.isDonePlaying;
	}
	
	@Override
	public FloatControl getControl(FloatControl.Type type)
	{
		return (FloatControl) this.line.getControl(type);
	}
	
	/* WARNING - Removed try catching itself - possible behaviour change. */
	public void appendAudioData(AudioInputStream ais) throws IOException
	{
		byte[] buffer = new byte[65536];
		int n22 = 0;
		while(n22 != -1)
		{
			if(this.isDisposed)
			{
				return;
			}
			if(this.isPaused)
			{
				Object object = this.lock;
				synchronized(object)
				{
					try
					{
						this.lock.wait();
					} catch(InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
			if(this.isDisposed)
			{
				return;
			}
			this.line.write(buffer, 0, n22);
			n22 = ais.read(buffer, 0, buffer.length);
		}
		synchronized(onFinished)
		{
			onFinished.notifyAll();
		}
		this.isDonePlaying = true;
		this.dispose();
	}
}
