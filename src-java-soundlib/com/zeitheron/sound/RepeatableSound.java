/*
 * Decompiled with CFR 0_123.
 */
package com.zeitheron.sound;

import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.zeitheron.sound.IInputStream;
import com.zeitheron.sound.ISound;
import com.zeitheron.sound.Sound;

public class RepeatableSound
implements ISound {
    private Sound unrepeatable;
    private float currentVol;
    private final IInputStream input;

    public RepeatableSound(IInputStream soundIn) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        this.input = soundIn;
        this.unrepeatable = new Sound(this.input.getInput());
    }

    public RepeatableSound(byte[] soundIn) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        this(new IInputStream.ByteArray((byte[])soundIn.clone()));
    }

    @Override
    public void setVolume(float vol) {
        this.currentVol = vol;
        if (this.unrepeatable != null) {
            this.unrepeatable.setVolume(vol);
        }
    }

    @Override
    public boolean isDonePlaying() {
        if (this.unrepeatable != null && !this.unrepeatable.isDonePlaying()) {
            return false;
        }
        return true;
    }

    @Override
    public void play() {
        if (this.unrepeatable != null) {
            this.unrepeatable.play();
            this.unrepeatable.setVolume(this.currentVol);
        } else {
            try {
                this.unrepeatable = new Sound(this.input.getInput());
                this.unrepeatable.setVolume(this.currentVol);
                this.unrepeatable.play();
            }
            catch (IOException | LineUnavailableException | UnsupportedAudioFileException exception) {
                // empty catch block
            }
        }
    }

    @Override
    public void stop() {
        if (this.unrepeatable != null) {
            this.unrepeatable.stop();
            this.unrepeatable.setVolume(this.currentVol);
        }
    }

    @Override
    public void dispose() {
        if (this.unrepeatable != null) {
            this.unrepeatable.dispose();
        }
        this.unrepeatable = null;
    }

    @Override
    public FloatControl getControl(FloatControl.Type type) {
        if (this.unrepeatable != null) {
            return this.unrepeatable.getControl(type);
        }
        throw new IllegalArgumentException("No sound currently playing!");
    }
}

